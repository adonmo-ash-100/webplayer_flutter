import 'dart:async';
import 'dart:io';

import 'package:cross_file/cross_file.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:media_kit/media_kit.dart';
import 'package:media_kit_video/media_kit_video.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:webplayer_flutter/constants.dart';
import 'package:webplayer_flutter/services/api.dart';
import 'package:webplayer_flutter/models/playunit.dart';
import 'package:webplayer_flutter/screens/home_screen.dart';
import 'package:webplayer_flutter/widgets/image_widget.dart';
import 'package:webplayer_flutter/widgets/video_widget.dart';

import 'package:socket_io_client/socket_io_client.dart' as io;
import 'package:webplayer_flutter/widgets/webview_widget.dart';
import 'package:webview_flutter/webview_flutter.dart';

class OnBoardingScreen extends StatefulWidget {
  const OnBoardingScreen({super.key});

  @override
  State<OnBoardingScreen> createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {
  late PlayerApi playerApi;
  late PlayUnit currentUnit;
  late Timer timer;
  int playLoopCtl = 0;
  String qrData = 'AdOnMO';

  late Player player = Player();
  late VideoController controller = VideoController(player);
  Image image = Image.asset('assets/setup_bg.png');

  WebViewController? webViewController;

  StreamSubscription? videoCompletedStatusStreamSubscription,
      videoPositionStreamSubscription;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    playerApi = PlayerApi(true);
    currentUnit = PlayUnit(
        code: "unit1",
        widget: "video",
        url: "assets/adspace_landscape.mp4",
        duration: 1000,
        fit: "cover");

    timer = Timer.periodic(Duration(milliseconds: playLoopCtl), (Timer _timer) {
      playLoop();
      setState(() {});
    });
    connectToSocket();
  }

  void connectToSocket() {
    var socket = io.io(API_ENDPONINT, {
      'transports': ['websocket'],
      'autoConnect': false
    });

    socket.connect();

    socket.onConnect((_) {
      print('connected');
    });

    socket.onDisconnect((_) {
      print('disconnected');
    });

    socket.on('loggedIn', (data) {
      print('logged in');
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => HomeScreen()));
    });
    Future.delayed(Duration(seconds: 5), () {
      socket.emit('login');
    });
  }

  void playLoop() async {
    PlayUnit nextUnit = await playerApi.getNextUnit();
    timer.cancel();
    playLoopCtl = nextUnit.duration;

    await player.stop();
    webViewController = null;
    if (nextUnit.widget == 'image') {
      timer =
          Timer.periodic(Duration(milliseconds: playLoopCtl), (Timer _timer) {
        playLoop();
      });
      await setUpImage(nextUnit);
    } else if (nextUnit.widget == 'video') {
      await setupVideoController(nextUnit);
    } else if (nextUnit.widget == 'webview') {
      timer =
          Timer.periodic(Duration(milliseconds: playLoopCtl), (Timer _timer) {
        playLoop();
      });
      await setupWebviewController(nextUnit);
    }
    setState(() {
      currentUnit = nextUnit;
    });
    print(currentUnit.url);
  }

  Future<void> setupWebviewController(PlayUnit _unit) async {
    if (webViewController == null) {
      webViewController = WebViewController();
    }
    await webViewController?.loadRequest(Uri.parse(_unit.url));
  }

  Future<void> setupVideoController(PlayUnit _unit) async {
    if (_unit.url.contains('assets') && Platform.isWindows == false) {
      var file = await rootBundle.load(_unit.url);
      var data = file.buffer.asUint8List();
      var media = await Media.memory(data);
      await player.open(media);
    } else {
      await player.open(Media(_unit.url));
    }
    videoCompletedStatusStreamSubscription =
        player.stream.completed.listen((completedStatus) {
      if (completedStatus) {
        videoCompletedStatusStreamSubscription?.cancel();
        videoPositionStreamSubscription?.cancel();
        playLoop();
      }
    });

    videoPositionStreamSubscription =
        player.stream.position.listen((currentPosition) {
      if (currentPosition.inMilliseconds == _unit.duration) {
        videoCompletedStatusStreamSubscription?.cancel();
        videoPositionStreamSubscription?.cancel();
        playLoop();
      }
    });
  }

  bool isUrl(String url) {
    return url.contains('http') && Uri.parse(url).isAbsolute;
  }

  Future<void> setUpImage(PlayUnit _unit) async {
    if (isUrl(_unit.url)) {
      setState(() {
        image = Image.network(_unit.url);
      });
    } else if (_unit.url.contains('assets')) {
      setState(() {
        image = Image.asset(_unit.url);
      });
    } else {
      XFile file = XFile((_unit.url));
      var x = await file.readAsBytes();
      setState(() {
        image = Image.memory(x);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(alignment: Alignment.center, children: [
        _getBody(),
        Positioned(
          bottom: 0,
          right: 0,
          child: Container(
            color: Colors.white,
            child: QrImageView(
              data: qrData,
              size: 150,
            ),
          ),
        ),
      ]),
    );
  }

  Widget _getBody() {
    setState(() {});
    print('video-play1');
    if (currentUnit.widget == 'image') {
      // XFile file = XFile(currentUnit.url);
      return Center(
        child: image,
      );
    } else if (currentUnit.widget == "video") {
      return Video(
        controller: controller,
        controls: (state) {
          return Container();
        },
      );
    } else if (currentUnit.widget == 'webview') {
      // return WebviewWidget(url: currentUnit.url);
      return Container(
          child: WebViewWidget(
        controller: webViewController!,
      ));
    } else
      return Center(child: Text('Unknown'));
  }

  @override
  void dispose() {
    // TODO: implement dispose
    player.dispose();
    super.dispose();
  }
}
