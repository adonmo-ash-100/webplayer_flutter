import 'package:hive/hive.dart';
import 'package:webplayer_flutter/models/playlist.dart';
import 'package:webplayer_flutter/models/playunit.dart';

class LocalStorageHelper {
  String localPlaylist = 'adn_local_playlist';

  PlayList getLocalPlaylist() {
    print('getting local playlist ...');
    var box = Hive.box(localPlaylist);
    PlayList playlist = box.get('playlist');
    print(playlist.units.length);
    print("local:" + playlist.units[0].url);
    return playlist;
  }

  void addUnitsToLocalPlaylist(List<PlayUnit> playunits) {
    var box = Hive.box(localPlaylist);
    print('addnig units to local playlist');
    PlayList playlist;
    if (box.containsKey('playlist') == false) {
      print('new playlist');
      playlist = PlayList(id: 'id', units: playunits);
    } else {
      playlist = box.get('playlist');
      for (var unit in playunits) {
        if (!checkUnitPresentInLocalPlaylist(unit)) {
          playlist.units.add(unit);
        }
      }
    }
    // box.put('randomplayunit', playunits[0]);
    box.put('playlist', playlist);
  }

  bool checkUnitPresentInLocalPlaylist(PlayUnit _unit) {
    var box = Hive.box(localPlaylist);
    PlayList playlist = box.get('playlist');
    for (var unit in playlist.units) {
      if (unit.code == _unit.code) {
        return true;
      }
    }
    return false;
  }

  bool checkLocalPlaylistPresent() {
    var box = Hive.box(localPlaylist);
    print('checking if local playlist present');
    if (box.containsKey('playlist') == false) return false;
    PlayList playList = box.get('playlist');
    if (playList.units.isEmpty) return false;
    print('local playlist present');
    return true;
  }
}
