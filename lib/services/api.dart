import 'package:webplayer_flutter/services/localStorageHelper.dart';
import 'package:webplayer_flutter/models/playlist.dart';
import 'package:webplayer_flutter/models/playlist_state.dart';
import 'package:webplayer_flutter/models/playunit.dart';

class PlayerApi {
  String API_ENDPOINT = "";
  late PlaylistState playerState;
  LocalStorageHelper localStorageHelper = LocalStorageHelper();
  late bool loadDefaultPlaylist;
  PlayerApi(bool _loadDefaultPlaylist) {
    loadDefaultPlaylist = _loadDefaultPlaylist;
    playerState =
        PlaylistState(currentUnitIndex: -1, activePlaylist: loadPlaylist());
  }
  PlayList loadPlaylist() {
    //check and load playlist from the local storage
    if (loadDefaultPlaylist) {
      return fallbackPlaylist();
    }
    if (localStorageHelper.checkLocalPlaylistPresent()) {
      return localStorageHelper.getLocalPlaylist();
    }
    return fallbackPlaylist();
  }

  PlayList fallbackPlaylist() {
    return PlayList(id: "1", units: [
      // PlayUnit(
      //     code: "unit002w",
      //     widget: "webview",
      //     url: "https://github.com/flutter",
      //     duration: 5000,
      //     fit: "cover"),
      // PlayUnit(
      //     code: "unit002w",
      //     widget: "webview",
      //     url: "https://www.google.co.in/",
      //     duration: 5000,
      //     fit: "cover"),
      // PlayUnit(
      //     code: "unit3",
      //     widget: "image",
      //     url: "assets/setup_bg.png",
      //     duration: 4000,
      //     fit: "cover"),
      // PlayUnit(
      //     code: "unit30",
      //     widget: "image",
      //     url: "https://picsum.photos/250?image=12",
      //     duration: 4000,
      //     fit: "cover"),
      // PlayUnit(
      //     code: "unit30",
      //     widget: "image",
      //     url: "https://picsum.photos/250?image=13",
      //     duration: 4000,
      //     fit: "cover"),
      PlayUnit(
          code: "unit001",
          widget: "video",
          url: "assets/adonmo_001.mp4",
          duration: 5000,
          fit: "cover"),
      PlayUnit(
          code: "unit002w",
          widget: "webview",
          url: "https://www.amazon.in/",
          duration: 10000,
          fit: "cover"),
      PlayUnit(
          code: "unit003",
          widget: "video",
          url: "assets/adonmo_003.mp4",
          duration: 5000,
          fit: "cover"),
      PlayUnit(
          code: "unit002",
          widget: "video",
          url: "assets/adspace_landscape.mp4",
          duration: 5000,
          fit: "cover"),
      // PlayUnit(
      //     code: "unit1",
      //     widget: "video",
      //     url:
      //         "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/WeAreGoingOnBullrun.mp4",
      //     duration: 2000,
      //     fit: "cover"),
      // PlayUnit(
      //     code: "unit4",
      //     widget: "video",
      //     url:
      //         "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4",
      //     duration: 2000,
      //     fit: "cover"),
    ]);
  }

  PlayUnit getNextUnit() {
    if (this.playerState.activePlaylist.units.isEmpty) {
      this.playerState.activePlaylist = this.fallbackPlaylist();
    }
    int nunits = this.playerState.activePlaylist.units.length;
    this.playerState.currentUnitIndex =
        ((this.playerState.currentUnitIndex + 1) % nunits);
    return this
        .playerState
        .activePlaylist
        .units[this.playerState.currentUnitIndex];
  }
}
