import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:webplayer_flutter/services/api.dart';
import 'package:dio/dio.dart';
import 'package:webplayer_flutter/models/playlist.dart';
import 'package:webplayer_flutter/models/playunit.dart';

class Videodownloadhelper {
  Future<String> downloadAndSaveVideo(PlayUnit playunit) async {
    Dio dio = Dio();
    try {
      Directory dir = await getApplicationCacheDirectory();
      String savePath = dir.path + '/' + playunit.code;
      await dio.download(playunit.url, savePath);
      return savePath;
    } catch (e) {
      return "error";
    }
  }

  Future<PlayList> download(PlayList playList) async {
    for (int i = 0; i < playList.units.length; i++) {
      PlayUnit playunit = playList.units[i];
      print('downloading...' + playunit.code);
      if (playunit.widget == 'webview') continue;
      await downloadAndSaveVideo(playunit).then((String val) {
        print(val);
        playunit.url = val;

        // save the video path locally as well so that it can be reused
      });
      playList.units[i] = playunit;
    }
    return playList;
  }
}
