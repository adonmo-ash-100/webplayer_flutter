import 'package:hive/hive.dart';
import 'package:webplayer_flutter/models/playunit.dart';

part 'playlist.g.dart';

@HiveType(typeId: 1)
class PlayList extends HiveObject {
  @HiveField(0)
  String id;
  @HiveField(1)
  List<PlayUnit> units;

  PlayList({required this.id, required this.units});

  factory PlayList.fromJson(Map<String, dynamic> data) {
    return PlayList(id: data['id'], units: getUnits(data['units']));
  }

  static List<PlayUnit> getUnits(List<dynamic> _data) {
    List<PlayUnit> _temp = [];
    for (var x in _data) {
      _temp.add(PlayUnit.fromJson(x));
    }
    return _temp;
  }

  dynamic toJson() {
    return {
      'id': id,
      'units': units.map((e) => e.toJson()).toList(),
    };
  }
}
