import 'package:hive/hive.dart';

part 'playunit.g.dart';

@HiveType(typeId: 0)
class PlayUnit extends HiveObject {
  @HiveField(0)
  String code;
  @HiveField(1)
  String widget;
  @HiveField(2)
  String url;
  @HiveField(3)
  int duration;
  @HiveField(4)
  String fit;

  PlayUnit(
      {required this.code,
      required this.widget,
      required this.url,
      required this.duration,
      required this.fit});

  factory PlayUnit.fromJson(Map<String, dynamic> data) {
    return PlayUnit(
        code: data['code'],
        widget: data['widget'],
        url: data['url'],
        duration: data['duration'],
        fit: data['fit']);
  }

  dynamic toJson() {
    return {
      'code': code,
      'widget': widget,
      'url': url,
      'duration': duration,
      'fit': fit,
    };
  }
}
