import 'package:webplayer_flutter/models/playlist.dart';

class PlaylistState {
  int currentUnitIndex;
  PlayList activePlaylist;

  PlaylistState({required this.currentUnitIndex, required this.activePlaylist});
}
