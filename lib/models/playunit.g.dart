// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'playunit.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PlayUnitAdapter extends TypeAdapter<PlayUnit> {
  @override
  final int typeId = 0;

  @override
  PlayUnit read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return PlayUnit(
      code: fields[0] as String,
      widget: fields[1] as String,
      url: fields[2] as String,
      duration: fields[3] as int,
      fit: fields[4] as String,
    );
  }

  @override
  void write(BinaryWriter writer, PlayUnit obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.code)
      ..writeByte(1)
      ..write(obj.widget)
      ..writeByte(2)
      ..write(obj.url)
      ..writeByte(3)
      ..write(obj.duration)
      ..writeByte(4)
      ..write(obj.fit);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PlayUnitAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
