import 'dart:io';

import 'package:cross_file/cross_file.dart';
import 'package:flutter/material.dart';
import 'package:webplayer_flutter/services/api.dart';

class ImageWidget extends StatefulWidget {
  ImageWidget({super.key, required this.url});
  String url;

  @override
  State<ImageWidget> createState() => _ImageWidgetState();
}

class _ImageWidgetState extends State<ImageWidget> {
  Image image = Image.asset('assets/setup_bg.png');
  bool isUrl(String url) {
    return Uri.parse(url).isAbsolute;
  }

  Future<void> setUpImage() async {
    if (isUrl(widget.url)) {
      setState(() {
        image = Image.network(widget.url);
      });
    } else if (widget.url.contains('assets')) {
      setState(() {
        image = Image.asset(widget.url);
      });
    } else {
      XFile file = XFile((widget.url));
      var x = await file.readAsBytes();
      setState(() {
        image = Image.memory(x);
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initialise();
  }

  void initialise() async {
    await setUpImage();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(18),
            ),
          ),
          child: image),
    );
  }
}
