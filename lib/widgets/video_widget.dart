import 'dart:io';

import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class VideoWidget extends StatefulWidget {
  VideoWidget({super.key, required this.url});
  String url;
  @override
  State<VideoWidget> createState() => _VideoWidgetState();
}

class _VideoWidgetState extends State<VideoWidget> {
  late VideoPlayerController controller;

  bool isUrl(String url) {
    return Uri.parse(url).isAbsolute;
  }

  void initialiseController() {
    if (widget.url.contains('assets/')) {
      controller = VideoPlayerController.asset(widget.url)
        ..initialize().then((_) {
          setState(() {
            controller.play();
          });
        });
    } else if (isUrl(widget.url)) {
      controller = VideoPlayerController.networkUrl(Uri.parse(widget.url))
        ..initialize().then((_) {
          setState(() {
            controller.play();
          });
        });
    } else {
      controller = VideoPlayerController.file(File(widget.url))
        ..initialize().then((_) {
          setState(() {
            controller.play();
          });
        });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print('video-play' + widget.url);
    initialiseController();
    // controller = VideoPlayerController.asset('assets/adspace_landscape.mp4')
    //   ..initialize().then((_) {
    //     setState(() {
    //       controller.play();
    //     });
    //   });
  }

  @override
  Widget build(BuildContext context) {
    return controller.value.isInitialized
        ? Center(
            child: AspectRatio(
              aspectRatio: controller.value.aspectRatio,
              child: VideoPlayer(controller),
            ),
          )
        : Center(
            child: Container(
              child: Text('No video' + widget.url),
            ),
          );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    // controller.pause();
    controller.dispose();
    super.dispose();
  }
}
